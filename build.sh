#!/bin/bash
source env.sh
# monkey patch so that bin2c won't complain about abnormal binary file size
sed 's/cbMax\ =\ 1024/cbMax\ =\ 4096/g' src/bldprogs/bin2c.c
kmk TOOL_YASM_AS=yasm VBOX_USE_SYSTEM_XORG_HEADERS=1 VBOX_USE_SYSTEM_GL_HEADERS=1 VBOX_NO_LEGACY_XORG_X11=1 VBOX_WITH_REGISTRATION_REQUEST= VBOX_WITH_UPDATE_REQUEST= KBUILD_VERBOSE=2 VBOX_WITH_EXTPACK_VBOXDTRACE= VBoxDD